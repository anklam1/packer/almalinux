packer {
  required_plugins {
    virtualbox = {
      version = "~> 1"
      source  = "github.com/hashicorp/virtualbox"
    }
    vagrant = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/vagrant"
    }
  }
}

source "virtualbox-iso" "almalinux" {
  guest_os_type = "RedHat_64"
  iso_url       = "https://repo.almalinux.org/almalinux/9.3/isos/x86_64/AlmaLinux-9.3-x86_64-minimal.iso"
  iso_checksum  = "file:https://repo.almalinux.org/almalinux/9.3/isos/x86_64/CHECKSUM"
  cpus          = 4
  memory        = 8192
  disk_size     = 20000
  # headless             = true
  http_directory = "http"
  # cd_files = ["./almalinux-9.ks"]
  # cd_label         = "cidata"
  shutdown_command     = "echo vagrant | sudo -S /sbin/shutdown -hP now"
  ssh_username         = "vagrant"
  ssh_password         = "vagrant"
  ssh_timeout          = "3600s"
  hard_drive_interface = "sata"
  iso_interface        = "sata"
  # firmware             = "efi"
  boot_wait = "5s"
  boot_command = [
    "<tab>",
    "<wait>",
    "linuxefi /images/pxeboot/vmlinuz",
    " inst.stage2=hd:LABEL=AlmaLinux-9-3-x86_64-dvd ro",
    " inst.text biosdevname=0 net.ifnames=0",
    " inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/almalinux-9.ks",
    # " linux ks=cdrom:/almalinux-9.ks",
    # " kvm --append 'autoinstall ds=nocloud;s=/cdrom/almalinux-9.ks'",
    "<enter>",
    "initrdefi /images/pxeboot/initrd.img",
    "<enter>",
    "boot<enter><wait>"
  ]
  vboxmanage = [
    ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"],
    #   ["modifyvm", "{{.Name}}", "--memory", 8192],
    #   ["modifyvm", "{{.Name}}", "--cpus", 8]
  ]
  vboxmanage_post = [
    ["modifyvm", "{{.Name}}", "--memory", 2048],
    ["modifyvm", "{{.Name}}", "--cpus", 1]
  ]
}

build {
  sources = ["sources.virtualbox-iso.almalinux"]

  post-processor "vagrant" {
  }
}